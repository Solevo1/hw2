const express = require('express');
const router = express.Router();

const {
    addNoteToUser,
    getNotesByUserId,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    checkNoteByIdForUser,
    deleteNoteByIdForUser
} = require('../services/notesService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const notes = await getNotesByUserId(userId,req.query.offset,req.query.limit);

    res.json({notes});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const note = await getNoteByIdForUser(id, userId);

    if (!note) {
        throw new InvalidRequestError('No note with such id found!');
    }

    res.json({note});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addNoteToUser(userId, req.body);

    res.json({message: "Note created successfully"});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    await updateNoteByIdForUser(id, userId, data);

    res.json({message: "Note updated successfully"});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await checkNoteByIdForUser(id, userId);

    res.json({message: "Note updated successfully"});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await deleteNoteByIdForUser(id, userId);
    
    res.json({message: "Note deleted successfully"});
}));

module.exports = {
    notesRouter: router
}
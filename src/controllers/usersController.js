const express = require('express');
const router = express.Router();

const {
    deleteUserById,
    updatePasswordById,
    getUserById
} = require('../services/usersService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId,username,createdDate } = req.user;
    const user = await getUserById(userId);
    console.log(user)
    res.json({user:{
        _id:userId,
        username,
        createdDate
    }});
}));

router.delete('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await deleteUserById(userId);

    res.json({message: "User deleted successfully"});
}));

router.patch('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    console.log(req.body);
    await updatePasswordById(userId,req.body.password)

    res.json({message: "Password updated successfully"});
}));



module.exports = {
    usersRouter: router
}